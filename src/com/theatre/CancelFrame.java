package com.theatre;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;


public class CancelFrame extends JFrame implements ActionListener{
	
	private int seatNo;
	private Theater theater;
	
	public CancelFrame(int seatNo, Theater theater){
		this.seatNo = seatNo;
		this.theater = theater;
		
		setLayout(null);
		setSize(450,150);
		setLocation(500,300);
		
		JLabel name = new JLabel("Are you sure, you want to Cancel the reservation?");
		name.setBounds(100, 20, 300, 30);
		getContentPane().add(name);

		JButton submit = new JButton("Confirm");
		submit.setSize(150, 30);
		submit.setLocation(120, 50);
		submit.addActionListener(this);
		getContentPane().add(submit);
	}


	public void actionPerformed(ActionEvent e) {
		theater.seatReservation[seatNo] = null;
		this.setVisible(false);
		theater.seatButtons[seatNo].setBackground(Color.GREEN);
		
	}

}

