package com.theatre;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class ReserveFrame extends JFrame implements ActionListener {

	private int seatNo;
	private Theater theater;

	public ReserveFrame(int seatNo, Theater theater) {
		this.seatNo = seatNo;
		this.theater = theater;
		setLayout(null);
		setSize(450, 150);
		setLocation(500, 300);

		JLabel name = new JLabel("Are you sure you want to Reserve this seat?");
		name.setBounds(100, 20, 300, 30);
		getContentPane().add(name);

		JButton submit = new JButton("Confirm");
		submit.setSize(150, 30);
		submit.setLocation(120, 50);
		submit.addActionListener(this);
		getContentPane().add(submit);
	}

	public void actionPerformed(ActionEvent ae) {
		this.theater.seatReservation[seatNo] = "R";
		System.out.println(((JButton) ae.getSource()).getText());
		(theater.seatButtons[seatNo]).setBackground(Color.RED);
		this.setVisible(false);

	}

}
