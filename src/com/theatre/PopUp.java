package com.theatre;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class PopUp extends JFrame {

	public PopUp(float cost) {

		setLayout(null);
		setSize(450, 150);
		setLocation(500, 300);

		JLabel name = new JLabel("Total Cost = " + cost );
		name.setBounds(160, 20, 300, 30);
		getContentPane().add(name);

	}

}
