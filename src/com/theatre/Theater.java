package com.theatre;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class Theater extends JFrame implements ActionListener {

	public String[] seatReservation;
	final int NO_OF_SEATS = 560;
	JButton[] seatButtons;

	public Theater() {
		seatReservation = new String[NO_OF_SEATS];
		seatButtons = new JButton[NO_OF_SEATS];

		setLocation(20, 20);
		setSize(1200, 700);
		setLayout(new FlowLayout());
		for (int i = 0; i < NO_OF_SEATS; i++) {
			seatButtons[i] = new JButton("" + i);
			seatButtons[i].setPreferredSize(new Dimension(36, 25));
			seatButtons[i].setBackground(Color.GREEN);
			seatButtons[i].addActionListener(this);
			getContentPane().add(seatButtons[i]);

		}

		JButton emptyAll = new JButton("Empty All Seats");
		emptyAll.setSize(100, 30);
		emptyAll.addActionListener(this);
		getContentPane().add(emptyAll);

		JButton reserveAll = new JButton("Reserve All Seats");
		reserveAll.setSize(100, 30);
		reserveAll.addActionListener(this);
		getContentPane().add(reserveAll);

		JButton totalCost = new JButton("Total Cost");
		totalCost.setSize(100, 30);
		totalCost.addActionListener(this);
		getContentPane().add(totalCost);
	}

	public void actionPerformed(ActionEvent ae) {
		final String source = ((JButton) ae.getSource()).getText();
		
		if (source.equals("Empty All Seats")) {
			for (int i = 0; i < NO_OF_SEATS; i++) {
				seatButtons[i].setBackground(Color.GREEN);
				seatReservation[i] = null;
			}

		} else if (source.equals("Reserve All Seats")) {
			for (int i = 0; i < NO_OF_SEATS; i++) {
				seatButtons[i].setBackground(Color.RED);
				seatReservation[i] = "R";
			}

		} else if (source.equals("Total Cost")) {
			int seats = 0;
			float totalCost = 0;
			for (int i = 0; i < NO_OF_SEATS; i++) {
				
				if(i<140){
					if (seatReservation[i] == "R") {
						totalCost += 18;
					}

				}else if (i>=140 && i<364){
					if (seatReservation[i] == "R") {
						totalCost += 18;
					}
				}else if(i>= 336){
					if (seatReservation[i] == "R") {
						totalCost += 18;
					}
				}
			}
			
			totalCost=totalCost +(totalCost*5/100);
			
			new PopUp(totalCost).setVisible(true);

		} else {
			int seatNo = Integer.parseInt(source);

			if (seatReservation[seatNo] == null) {
				JFrame reserveWindow = new ReserveFrame(seatNo, this);
				reserveWindow.setVisible(true);
			} else {
				JFrame cancelWindow = new CancelFrame(seatNo, this);
				cancelWindow.setVisible(true);
			}
		}
	}
}
