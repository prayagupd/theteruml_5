package com.theatre;
import java.util.Calendar;


public class TheaterApplication {
	
	public static void main(String args[]){
		
		Theater theater = new Theater();
		theater.setVisible(true);
		
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK);
		
		if(day == 1 || day == 7) {
			Theater theater2 = new Theater();
			theater2.setVisible(true);
		}
	}
}
